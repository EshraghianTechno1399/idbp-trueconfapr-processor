package com.tosantechno.idbp.trueconfapi.controller;

import com.google.gson.Gson;
import com.tosantechno.idbp.trueconfapi.config.Properties;
import com.tosantechno.idbp.trueconfapi.model.ObjectUser;
import com.tosantechno.idbp.trueconfapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by e.eshraghian on 19/08/2020.
 */
@RestController
public class UserController
{

    @Autowired
    private ObjectUser objectUser;

    @Autowired
    private Properties properties;




    @PostMapping("/createUser")
    public User createUser(@RequestBody ObjectUser inputObjectUser) throws IOException
    {

        ObjectUser Obj = fillUserObjectFromParameters(inputObjectUser);
        String jsonStr = new Gson().toJson(Obj);

        String URLStatement = properties.getServer_address()
                    + properties.getCreate_user_url().toString() + properties.getAccess_token();
        URL obj = new URL(URLStatement);
        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setDoOutput(true);
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("Content-Type", "application/json");

        OutputStream os = postConnection.getOutputStream();
        os.write(jsonStr.getBytes());
        os.flush();

        int responseCode = postConnection.getResponseCode();
        System.out.println("POST Response Code :  " + responseCode);
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());
        if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK)
        { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();
            // print result
            //System.out.println(response.toString());
            //return response.toString();
            return new Gson().fromJson(response.toString(), User.class);
        } else
        {
            //Fail
           //  System.out.println("POST NOT WORKED");
            //return "POST NOT WORKED";
            return new User();
        }

    }

    private ObjectUser fillUserObjectFromParameters(ObjectUser inputObjectUser)
    {
        objectUser.setId(inputObjectUser.getId());
        objectUser.setUid(inputObjectUser.getUid());
        objectUser.setAvatar(inputObjectUser.getAvatar());
        objectUser.setLogin_name(inputObjectUser.getLogin_name());
        objectUser.setPassword(inputObjectUser.getPassword());
        objectUser.setEmail(inputObjectUser.getEmail());
        objectUser.setDisplay_name(inputObjectUser.getDisplay_name());
        objectUser.setFirst_name(inputObjectUser.getFirst_name());
        objectUser.setLast_name(inputObjectUser.getLast_name());
        objectUser.setCompany(inputObjectUser.getCompany());
        objectUser.setGroups(inputObjectUser.getGroups());
        objectUser.setMobile_home(inputObjectUser.getMobile_home());
        objectUser.setWork_home(inputObjectUser.getWork_home());
        objectUser.setHome_phone(inputObjectUser.getHome_phone());
        objectUser.setStatus(inputObjectUser.getStatus());
        objectUser.setIs_active(inputObjectUser.getIs_active());
        return objectUser;
    }
}
