package com.tosantechno.idbp.trueconfapi.controller;

import com.google.gson.Gson;
import com.tosantechno.idbp.trueconfapi.config.Properties;
import com.tosantechno.idbp.trueconfapi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by user on 8/16/2020.
 */
@RestController
public class ConferenceController {

    @Autowired
    private ObjectConference objectConference;

    @Autowired
    private Properties properties;

    @GetMapping("/getConferences/{conference_id}")
    public String getConferences(@PathVariable String conference_id) {
        try {
            String URLStatement = properties.getServer_address()
                    + properties.getGet_conference_url().toString() + "/" + conference_id
                    +"?access_token=" + properties.getAccess_token();
            URL url = new URL(URLStatement);
            HttpURLConnection getConnection = (HttpURLConnection) url.openConnection();
            getConnection.setRequestMethod("GET");
            getConnection.setRequestProperty("Accept", "application/json");

            if (getConnection.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + getConnection.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (getConnection.getInputStream())));

            String output;
            String str = "";
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                str = str+ output;
            }

            getConnection.disconnect();
            return str;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }


    @PostMapping("/createConference")
    public Conference createConference(@RequestBody ObjectConference inputObjectConference) throws IOException {
        ObjectConference Obj = fillConferenceObjectFromParameters(inputObjectConference);
        String jsonStr = new Gson().toJson(Obj);
        // Displaying JSON String
        //System.out.println(jsonStr);

        String URlStatement = properties.getServer_address()
                    + properties.getCreate_conference_url().toString() + properties.getAccess_token();
        URL obj = new URL(URlStatement);
        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setDoOutput(true);
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("Content-Type", "application/json");


        OutputStream os = postConnection.getOutputStream();
        os.write(jsonStr.getBytes());
        os.flush();

        int responseCode = postConnection.getResponseCode();
        System.out.println("POST Response Code :  " + responseCode);
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());
        if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK)
        { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();
            // print result
            System.out.println(response.toString());
            //return response.toString();
            return new Gson().fromJson(response.toString(), Conference.class);
        } else
        { //Fail
            System.out.println("POST NOT WORKED");
            return new Conference();
        }
    }


    @PostMapping("/runConference/{conference_id}")
    public String runConference (@PathVariable String conference_id) throws IOException
    {
        String URlStatement = properties.getServer_address()
                + properties.getRun_conference_url().toString() + "/"+ conference_id
                +"/run?access_token=" + properties.getAccess_token();
        URL obj = new URL(URlStatement);
        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setDoOutput(true);
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("Content-Type", "application/json");

        OutputStream os = postConnection.getOutputStream();
        os.flush();

        int responseCode = postConnection.getResponseCode();
        System.out.println("POST Response Code :  " + responseCode);
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());
        if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK)
        { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();
            // print result
            System.out.println(response.toString());
            return response.toString();
        } else
        { //Fail
            System.out.println("POST NOT WORKED");
            return "POST NOT WORKED";
        }

    }

    @PostMapping("/stopConference/{conference_id}")
    public String stopConference(@PathVariable String conference_id) throws IOException {
        String URlStatement = properties.getServer_address()
                + properties.getStop_conference_url().toString() + "/"+ conference_id
                +"/stop?access_token=" + properties.getAccess_token();

        URL obj = new URL(URlStatement);
        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setDoOutput(true);
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("Content-Type", "application/json");

        OutputStream os = postConnection.getOutputStream();
        os.flush();

        int responseCode = postConnection.getResponseCode();
        System.out.println("POST Response Code :  " + responseCode);
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());
        if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK)
        { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();
            // print result
            System.out.println(response.toString());
            return response.toString();
        } else
        { //Fail
            System.out.println("POST NOT WORKED");
            return "POST NOT WORKED";
        }
    }

    @GetMapping("/sharedConference/{conference_id}")
    public String sharedConference(@PathVariable String conference_id)
    {
        try {
            String URLStatement = properties.getServer_address()
                    + properties.getShared_conference_url().toString() + "/" + conference_id
                    + "/shared?access_token="
                    + properties.getAccess_token();
            URL url = new URL(URLStatement);
            HttpURLConnection getConnection = (HttpURLConnection) url.openConnection();
            getConnection.setRequestMethod("GET");
            getConnection.setRequestProperty("Accept", "application/json");

            if (getConnection.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + getConnection.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (getConnection.getInputStream())));

            String output;
            String str = "";
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                str = str+ output;
            }

            getConnection.disconnect();
            return str;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e)
        {
            e.printStackTrace();

        }
        return null;

    }


    @RequestMapping(value = "/inviteParticipant/{conference_id}" , method = RequestMethod.POST)
    public Participant inviteParticipant(@PathVariable String conference_id , @RequestBody ObjectUserParticipant participant_id) throws IOException {

        String URlStatement = properties.getServer_address()
                + properties.getInvite_participant_url().toString() + "/"+ conference_id
                +"/participants?access_token=" + properties.getAccess_token();

        String jsonStr = new Gson().toJson(participant_id);
        URL obj = new URL(URlStatement);
        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setDoOutput(true);
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("Content-Type", "application/json");

        OutputStream os = postConnection.getOutputStream();
        os.write(jsonStr.getBytes());
        os.flush();

        int responseCode = postConnection.getResponseCode();
        System.out.println("POST Response Code :  " + responseCode);
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());
        if (responseCode == HttpURLConnection.HTTP_CREATED || responseCode == HttpURLConnection.HTTP_OK)
        { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in.close();
            // print result
            System.out.println(response.toString());
           // return response.toString();
            return new Gson().fromJson(response.toString(), Participant.class);
        } else
        { //Fail
          //  System.out.println("POST NOT WORKED");
            return new Participant();
        }
    }
    private ObjectConference fillConferenceObjectFromParameters(ObjectConference inputObjectConference)
    {
        objectConference.setId(inputObjectConference.getId());
        objectConference.setTopic(inputObjectConference.getTopic());
        objectConference.setDescription(inputObjectConference.getDescription());
        objectConference.setOwner(inputObjectConference.getOwner());
        objectConference.setType(inputObjectConference.getType());
        objectConference.setInvitations(inputObjectConference.getInvitations());
        objectConference.setMax_podiums(inputObjectConference.getMax_podiums());
        objectConference.setMax_participants(inputObjectConference.getMax_participants());
        objectConference.setSchedule(inputObjectConference.getSchedule());
        objectConference.setAllow_guests(inputObjectConference.getAllow_guests());
        objectConference.setRights(inputObjectConference.getRights());
        objectConference.setAuto_invite(inputObjectConference.getAuto_invite());
        objectConference.setUrl(inputObjectConference.getUrl());
        objectConference.setWebclient_url(inputObjectConference.getWebclient_url());
        objectConference.setState(inputObjectConference.getState());
        objectConference.setTags(inputObjectConference.getTags());
        objectConference.setRecordings(inputObjectConference.getRecordings());
        return objectConference;
    }


}
