package com.tosantechno.idbp.trueconfapi.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by e.eshraghian on 22/08/2020.
 */
@Getter
@Setter
public class ObjectParticipant
{
   private    String      id;
    private    String      uid;
    private    String      avatar;
    private    String      login_name;
    private    String      email;
    private    String      display_name;
    private    String      first_name;
    private    String      last_name;
    private    String      company;
    private    Object[]    groups;
    private    String      mobile_home;
    private    String      work_home;
    private    String      home_phone;
    private    Integer     status;
    private    Integer     is_active;
    private    String      join_time;
    private    boolean     is_owner;

}
