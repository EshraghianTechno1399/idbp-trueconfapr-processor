package com.tosantechno.idbp.trueconfapi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;

/**
 * Created by user on 8/16/2020.
 */
@Getter
@Setter
@Component
public class ObjectConference
{
    private String    id;               //Unique conference identifier , Size range: 1..128
    private String    topic;            //Conference subject, Size range: 1..240
    private String    description;      //Conference description.
    private String    owner;            //User identifier (user_id) of the conference owner, Size range: 1..64
    private Integer   type;             //0 - symmetric , 1 - asymmetric, 3 - role-based
    private Object[]  invitations;      //Invitations list. They are used to automatically invite to the conferences. List of ObjectInvitationMini.
    private Integer   max_podiums;      //The maximum number of conference speakers.
    private Integer   max_participants; //Maximum number of simultaneous conference participants. Maximum number depends on the server license and conference type
    private Object    schedule;         //Schedule object.
    private Boolean   allow_guests;     //Permission to invite guests to the conference. For editing you need a webinar license.
    private Object[]  rights;            //ClientRights objects.
    private Integer   auto_invite;      //his parameter is responsible for sending automatic invitations at the conference start:
    private String    url;              //Conference page.
    private String    webclient_url;    //Conference widget.
    private String    state;            //Conference state.
    private Array     tags;             //Conference tags for fast search.
    private String    recordings;       //Conference recording state.


}
