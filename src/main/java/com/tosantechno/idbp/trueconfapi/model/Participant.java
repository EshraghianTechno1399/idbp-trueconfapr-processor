package com.tosantechno.idbp.trueconfapi.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by e.eshraghian on 23/08/2020.
 */
@Getter
@Setter
public class Participant
{
    private ObjectParticipant participant;
}
