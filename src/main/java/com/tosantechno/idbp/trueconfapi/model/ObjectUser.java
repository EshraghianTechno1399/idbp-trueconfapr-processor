package com.tosantechno.idbp.trueconfapi.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * Created by e.eshraghian on 19/08/2020.
 */
@Getter
@Setter
@Component
public class ObjectUser
{
     private    String      id;             //User unique identifier.
     private    String      uid;            //User unique identifier for calls.
     private    String      avatar;         //User avatar address. If there is no avatar, null is returned.
     private    String      login_name;     //User username. Size range: 1..32
     private    String      password;       //User password. Size range: 1..255
     private    String      email;          //User unique email address. Size range: 3..250
     private    String      display_name;   //User display name. Size range: 1..64
     private    String      first_name;     //User first name. Size range: 0..64
     private    String      last_name;      //User last name. Size range: 0..64
     private    String      company;        //User company. Size range: 0..64
     private    Object[]    groups;         //List of ObjectGroupMini.
     private    String      mobile_home;    //User mobile phone. Size range: 0..50
     private    String      work_home;      //User work phone. Size range: 0..50
     private    String      home_phone;     //User home phone. Size range: 0..50
     private    Integer     status;         //NOT_ACTIVE: -2, INVALID: -1, OFFLINE: 0, ONLINE: 1, BUSY: 2, MULTIHOST: 5 (The user is in the conference and is the conference owner).
     private    Integer     is_active;      //Account status: 1 - enabled, 0 - disabled.

}
