package com.tosantechno.idbp.trueconfapi.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by e.eshraghian on 19/08/2020.
 */
@ConfigurationProperties("properties")
@Setter
@Getter
public class Properties
{
    private  String     access_token;
    private  String     server_address;
    private  String     create_conference_url;
    private  String     get_conference_url;
    private  String     create_user_url;
    private  String     run_conference_url;
    private  String     stop_conference_url;
    private  String     shared_conference_url;
    private  String     invite_participant_url;
}
