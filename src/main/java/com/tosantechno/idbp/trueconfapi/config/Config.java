package com.tosantechno.idbp.trueconfapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by user on 8/16/2020.
 */
@Configuration
public class Config
{

    @Bean
    public Properties getProperties() { return new Properties();}

}
